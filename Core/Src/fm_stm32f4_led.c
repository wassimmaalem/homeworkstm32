// Adapted from the STM32F4-Discovery project

/***************************************************************
* File     : fm_stm32f4_led.c
* website  : www.firmcodes.com
* email_id : support@firmcodes.com
* IDE used : The files are Compiled in coocox ide and
			 tested on stm32f4 discovery board.
***************************************************************/
#include "fm_stm32f4_led.h"
#include "time.h"



/***************************************************************
* All LED on Stm32f4 discovery board
***************************************************************/
LED_struct LED[] = {
  //Name, PORT , PIN       , CLOCK            , Init
  {LED_1, GPIOA, GPIO_PIN_5,RCC_AHB1ENR_GPIOAEN ,LED_OFF},
  //{LED_1,GPIOD,GPIO_Pin_12,RCC_AHB1Periph_GPIOD,LED_OFF},   // PD12=Green LED on Discovery-Board
  //{LED_2,GPIOD,GPIO_Pin_13,RCC_AHB1Periph_GPIOD,LED_OFF},   // PD13=Orange LED on Discovery-Board
  //{LED_3,GPIOD,GPIO_Pin_14,RCC_AHB1Periph_GPIOD,LED_OFF},   // PD14=Red LED on Discovery-Board
  //{LED_4,GPIOD,GPIO_Pin_15,RCC_AHB1Periph_GPIOD,LED_OFF},   // PD15=Blue LED on Discovery-Board
};

/***************************************************************
* Initialize all LED
***************************************************************/
void FM_Led_Init(void)
{
  GPIO_InitTypeDef  GPIO_InitStructure;
  LED_NAME led_name;

    // Config as digital output
    GPIO_InitStructure.Pin = GPIO_PIN_5;
    GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP ;
    GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(LED[led_name].LED_PORT, &GPIO_InitStructure);

  }



/***************************************************************
* LED off
***************************************************************/
void FM_Led_Off(LED_NAME led_name)
{
  LED[led_name].LED_PORT->BSRR = LED[led_name].LED_PIN;
}

/***************************************************************
* LED on
***************************************************************/
void FM_Led_On(LED_NAME led_name)
{
  LED[led_name].LED_PORT->BSRR = LED[led_name].LED_PIN;
  HAL_Delay(1000);
}

/***************************************************************
* LED toggle
***************************************************************/
void FM_Led_Toggle(LED_NAME led_name)
{

	LED[led_name].LED_PORT->ODR ^= LED[led_name].LED_PIN;

	HAL_Delay(1000);

}

double Switch_Led(LED_NAME led_name)
{

	clock_t begin = clock();

	LED[led_name].LED_PORT->ODR ^= LED[led_name].LED_PIN;

	clock_t end = clock();
	double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;

	HAL_Delay(100);
	return time_spent ;
}
