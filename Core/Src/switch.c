/*
 * hello.c
 *
 *  Created on: Sep 28, 2020
 *      Author: zamek
 */

#include <string.h>
#include "cmsis_os.h"
#include "fm_stm32f4_led.h"
#include "time.h"
#include "switch.h"
#include "stdbool.h"
#include "shell.h"
#include <assert.h>

#define MIN_VALUE 0
#define MAX_VALUE 400
#define CMD_ARG_SET_ON "on"
#define CMD_ARG_SET_OFF "off"
#define CMD_ARG_SET_INFO "info"
#define CMD_ARG_SET_AVERAGE "average"


#define TASK_DELAY_MS 1000

static struct {
	bool initialized;
	uint16_t value;
	uint16_t on;
	uint16_t off;
	uint16_t count;
	double val;

} hello_control;


BaseType_t hello_init(){
    FM_Led_Init();
	hello_control.initialized = true;
	hello_control.value = MIN_VALUE;
	hello_control.on = MIN_VALUE;
	hello_control.off = MIN_VALUE;
	hello_control.count = 0;
	hello_control.val = 0.0;
	return pdTRUE;
}

BaseType_t hello_deinit(){
	return pdTRUE;
}

void hello_function(const void * args){
	for(;;) {
		hello_control.value=hello_control.value < MAX_VALUE
				? hello_control.value+1
				: MIN_VALUE;
		osDelay(TASK_DELAY_MS);
	}
}


static void cmd_usage() {
	sh_printf("usage:%s [set <value>] | get]\r\n", HELLO_CMD);
}


void cmd_hello(int argc, char *argv[]){
	if (!hello_control.initialized) {
		sh_printf("hello is not initialized yet\r\n");
		return;
	}

	if (argc<1) {
		cmd_usage();
		return;
	}

	if (strcmp(argv[0], CMD_ARG_SET_ON)==0) {
		if (hello_control.value == 0){
				hello_control.val=Switch_Led(LED_1);
		        hello_control.count++;
		        hello_control.on++;
		        HAL_Delay(1000);
		        hello_control.value=1;
			sh_printf("Led Switched On !");
			HAL_Delay(1000);
		    }else{
		    	hello_control.count++;
		    	hello_control.on++;
			sh_printf("Led Already On !"); }
		return;
	}

	if (strcasecmp(argv[0], CMD_ARG_SET_OFF)==0) {
		if (hello_control.value == 1){
			hello_control.val=Switch_Led(LED_1);
			hello_control.count++;
			hello_control.off++;
			HAL_Delay(1000);
			hello_control.value=0;
			sh_printf("Led Switched OFF !");
			HAL_Delay(1000);
		    }else{
		    	hello_control.count++;
		    	hello_control.off++;
			sh_printf("Led Already OFF !");}
		return;
	}
	if (strcasecmp(argv[0], CMD_ARG_SET_AVERAGE)==0) {
		sh_printf("LED has Switched %d Times",hello_control.count);
		sh_printf("LED has Switched On %d Times",hello_control.on);
		sh_printf("LED has Switched Off %d Times",hello_control.off);
			return;
		}
	if (strcasecmp(argv[0], CMD_ARG_SET_INFO)==0) {
			int msec;
			msec = hello_control.val ;
			sh_printf("LED Last Switch Debounce counter is  %d at every 1 milliseconds ", msec%1000);

				return;
			}

	sh_printf("Unknown command:%s\r", argv[0]);
	cmd_usage();
}


