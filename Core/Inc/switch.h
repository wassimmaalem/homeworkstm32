
#define INC_HELLO_H_

#include "FreeRTOS.h"
#include "shell.h"

#define HELLO_CMD "switch"

#define HELLO_COMMANDS \
{ .command_name=HELLO_CMD, .function=cmd_hello }

void cmd_hello(int argc, char **argv);

BaseType_t hello_init();

BaseType_t hello_deinit();
